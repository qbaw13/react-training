import React, { Component } from 'react';
import { BrowserRouter as Router, Link, Route } from 'react-router-dom';
import { ToastContainer } from 'react-toastify';
import SaladMaker from './views/SaladMaker';
import OrderForm from './views/OrderForm'
import 'react-toastify/dist/ReactToastify.css';
import './App.scss';
import './global.scss'

class App extends Component {
  render() {
    return (
      <Router>
        <div className="app">
          <ToastContainer />
          <h1 className="app__logo"><Link className="app__logo-link" to="/">Salad Maker</Link></h1>
          <Route exact path="/" component={SaladMaker} />
          <Route exact path="/order-form" component={OrderForm} />
        </div>
      </Router>
    );
  };
};

export default App;
