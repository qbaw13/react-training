import React from 'react';
import './Message.scss';

export const INFO = 'INFO';
export const ERROR = 'ERROR';

const messageType = {
  [INFO]: 'message--info',
  [ERROR]: 'message--error',
};

const mapMessageType = type => messageType[type];

export default ({children, show, type}) => {
  if (!show) return null;

  return (
    <div className={`message ${mapMessageType(type)}`}>
      {children}
    </div>
  );
};