import React, { Component } from 'react';
import fetchIngredients from '../services/fetchIngredients';
import './IngredientsHandler.scss';
import Message, { ERROR, INFO } from './Message';

class IngredientsHandler extends Component {
  state = {
    ingredients: [],
    isFetching: true,
    isError: false
  };

  componentDidMount() {
    fetchIngredients()
      .then(this.handleSuccessResponse)
      .catch(this.handleErrorResponse);
  }

  handleSuccessResponse = response => {
    this.setState({
      ingredients: response.data,
      isFetching: false
    });
  };

  handleErrorResponse = response => {
    this.setState({
      isError: true,
      isFetching: false
    });
  };

  render() {
    const { ingredientAdd, ingredientRemove } = this.props;

    return (
      <div className="ingredients-handler">
        <Message show={this.state.isFetching} type={INFO}>
          Fetching
        </Message>
        <Message show={this.state.isError} type={ERROR}>
          Error
        </Message>
        <ul className="ingredients-handler__list list-unstyled">
          {this.state.ingredients.map((ingredient, index) => (
            <li className="ingredients-handler__list-item" key={index}>
              <span>{ingredient.label}</span>
              <div className="ingredients-handler__buttons">
                <button className="button button--sm" onClick={() => ingredientAdd(ingredient.type)}> + </button>
                {' '}
                <button className="button button--sm" onClick={() => ingredientRemove(ingredient.type)}> - </button>
              </div>
            </li>
          ))}
        </ul>
      </div>
    );
  };
};

export default IngredientsHandler;
