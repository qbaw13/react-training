import axios from 'axios';
import { API_BASE_URL } from './constants';

export default () => {
  return axios.get(`${API_BASE_URL}/ingredients`);
}