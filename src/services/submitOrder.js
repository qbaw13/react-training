import axios from 'axios';
import { API_BASE_URL } from './constants';

export default order => {
  return axios.post(`${API_BASE_URL}/order`, order);
}