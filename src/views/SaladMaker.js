import React, { Component } from 'react';
import Salad from '../components/Salad';
import prepareIngredients from '../utils/prepareIngredients';
import IngredientsHandler from '../components/IngredientsHandler';
import './SaladMaker.scss';

class SaladMaker extends Component {
  state = {
    ingredients: {}
  };

  ingredientAdd = type => {
    const updatedIngredientCount = this.doesIngredientExist(type)
      ? this.state.ingredients[type] + 1
      : 1;
    this.updateIngredientState(type, updatedIngredientCount);
  };

  ingredientRemove = type => {
    if (this.state.ingredients[type] === 0) return;
    const updatedIngredientCount = this.doesIngredientExist(type)
      ? this.state.ingredients[type] - 1
      : 0;
    this.updateIngredientState(type, updatedIngredientCount);
  };

  orderSalad = () => {
    this.setState({ isReadyToOrder: true });
  };

  updateIngredientState = (type, ingredientCount) => {
    this.setState({
      ingredients: {
        ...this.state.ingredients,
        [type]: ingredientCount
      }
    });
  };

  doesIngredientExist = type =>
    typeof this.state.ingredients[type] !== 'undefined';

  navigateToOrderForm = () => {
    this.props.history.push({
      pathname: '/order-form',
      state: {
        ingredients: this.state.ingredients
      }
    });
  };

  isOrderComplete = () => prepareIngredients(this.state.ingredients).length > 0;

  render() {
    const preparedIngredients = prepareIngredients(this.state.ingredients);

    return (
      <div className="salad-maker">
        <div className="salad-maker__panel salad-maker__panel--left">
          <Salad ingredients={preparedIngredients} />
        </div>
        <div className="salad-maker__panel salad-maker__panel--right">
          <IngredientsHandler
            ingredientAdd={this.ingredientAdd}
            ingredientRemove={this.ingredientRemove}
          />
          <button
            className="button button--box"
            onClick={this.navigateToOrderForm}
            disabled={!this.isOrderComplete()}
          >
            Make order
          </button>
        </div>
      </div>
    );
  }
}

export default SaladMaker;
