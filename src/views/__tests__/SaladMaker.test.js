import React from 'react';
import { shallow as render } from 'enzyme'; 
import SaladMaker from '../SaladMaker';
import IngredientsHandler from '../../components/IngredientsHandler';
import Salad from '../../components/Salad';
import OrderForm from '../../components/OrderForm'

describe('SaladMaker', () => {
    it('should render', () => {
        const sut = createSut();

        expect(sut.isEmptyRender()).toEqual(false);
    });

    describe('salad ingredients', () => {
        const INGREDIENT = 'INGREDIENT';
        let sut;

        beforeEach(() => {
            sut = createSut();
        });

        it('should pass empty ingredients list by default', () => {
            expect(sut.find(Salad).props().ingredients).toEqual([]);
        });

        it('should pass one specified ingredient when such ingredient is incremented and not exist', () => {
            sut.setState({ ingredients: {} });
            sut.find(IngredientsHandler).props().ingredientAdd(INGREDIENT);

            expect(sut.find(Salad).props().ingredients).toEqual(expect.arrayContaining([INGREDIENT]));
        });

        it('should pass incremented count of specified ingredient when such ingredient is incremented', () => {
            sut.setState({ ingredients: { [INGREDIENT]: 1 } });
            sut.find(IngredientsHandler).props().ingredientAdd(INGREDIENT);

            expect(sut.find(Salad).props().ingredients).toEqual(expect.arrayContaining([INGREDIENT, INGREDIENT]));
        });

        it('should not pass any ingredient when decremented ingredient not exist', () => {
            sut.setState({ ingredients: {} });
            sut.find(IngredientsHandler).props().ingredientRemove(INGREDIENT);

            expect(sut.find(Salad).props().ingredients).toEqual(expect.arrayContaining([]));
        });

        it('should pass decremented count of specified ingredient when such ingredient is decremented', () => {
            sut.setState({ ingredients: { [INGREDIENT]: 3 } });
            sut.find(IngredientsHandler).props().ingredientRemove(INGREDIENT);

            expect(sut.find(Salad).props().ingredients).toEqual(expect.arrayContaining([INGREDIENT, INGREDIENT]));
        });


        it('should not pass any ingredient when decremented ingredient count is 0', () => {
            sut.setState({ ingredients: { [INGREDIENT]: 0 } });
            sut.find(IngredientsHandler).props().ingredientRemove(INGREDIENT);

            expect(sut.find(Salad).props().ingredients).toEqual(expect.arrayContaining([]));
        });
    });

    describe('order form', () => {
        let sut;

        beforeEach(() => {
            sut = createSut();
        });

        it('should not be visible by default', () => {
            expect(sut.find(OrderForm).exists()).toBe(false);
        });

        it('should be visible when order button is clicked', () => {
            sut.find('button').simulate('click');
    
            expect(sut.find(OrderForm).exists()).toBe(true);
        });
    });

});

const createSut = () => render(<SaladMaker />);


