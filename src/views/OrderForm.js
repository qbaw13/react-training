import React, { Component } from 'react';
import { toast } from 'react-toastify';
import submitOrder from '../services/submitOrder';
import './OrderForm.scss';

class OrderForm extends Component {
  state = {
    name: '',
    address: '',
    phone: '',
    email: ''
  };

  handleSubmit = event => {
    const { name, address, phone, email } = this.state;
    event.preventDefault();
    submitOrder({
      customer: { name, address, phone, email },
      order: this.props.history.location.state.ingredients
    })
      .then(response => {
        toast.success(response.data.message);
      })
      .catch(error => {
        toast.error(error.response.data.message);
      });
  };

  handleInputChange = event => {
    const target = event.target;
    const name = target.name;

    this.setState({
      [name]: target.value
    });
  };

  render() {
    return (
      <form className="order-form" onSubmit={this.handleSubmit}>
        <div className="order-form__group">
          <label className="order-form__label">Name</label>
          <input
            className="order-form__input"
            name="name"
            value={this.state.name}
            onChange={this.handleInputChange}
          />
        </div>
        <div className="order-form__group">
          <label className="order-form__label">Address</label>
          <input
            className="order-form__input"
            name="address"
            value={this.state.address}
            onChange={this.handleInputChange}
          />
        </div>
        <div className="order-form__group">
          <label className="order-form__label">Phone</label>
          <input
            className="order-form__input"
            name="phone"
            value={this.state.phone}
            onChange={this.handleInputChange}
          />
        </div>
        <div className="order-form__group">
          <label className="order-form__label">Email</label>
          <input
            className="order-form__input"
            name="email"
            value={this.state.email}
            onChange={this.handleInputChange}
          />
        </div>
        <button className="button" type="submit">Submit</button>
      </form>
    );
  }
}

export default OrderForm;
